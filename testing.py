import rotator_cuff as rc
import visualization as vis
import networkx as nx
import cProfile
import time
import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

healthy_patients = ['ARC_CN_F01', 'ARC_CN_F03', 'ARC_CN_F04', 'ARC_CN_F05',
    'ARC_CN_F07', 'ARC_CN_M01', 'ARC_CN_M02', 'ARC_CN_M03', 'ARC_CN_M04',
    'ARC_CN_M05']
unhealthy_patients = ['ARC_RT_F07', 'ARC_RT_F09', 'ARC_RT_F12', 'ARC_RT_M05',
    'ARC_RT_M06', 'ARC_RT_M07']

sample = ['ARC_CN_F01', 'ARC_CN_M01','ARC_RT_F07','ARC_RT_M05']

def get_axis():
    xdims = []
    ydims = []
    zdims = []
    contents = rc.get_content(rc.dataSet)

    # Iterate through data set
    print time.strftime("%d/%m/%Y")
    for subject in unhealthy_patients:
        print "Subject: {}".format(subject)
        for muscle in rc.muscles[2:3]:
            #print "Muscle: {}".format(muscle)
            array = contents[subject][0][0][muscle][0][0]['dataPoints'][0][0]
            dims = rc.get_dimensions( array )
            xdims.append(dims[0][0])
            xdims.append(dims[1][0])
            ydims.append(dims[0][1])
            ydims.append(dims[1][1])
            zdims.append(dims[0][2])
            zdims.append(dims[1][2])

    print min(xdims), max(xdims)
    print min(ydims), max(ydims)
    print min(zdims), max(zdims)


def test():
    data = np.fromfile("tests/sliver5.txt", sep=" ")
    value = []
    array = []
    for d in data: 
        value.append(d)
        if len(value) == 4:
            array.append(value)
            value = []

    array = np.array(array)
    norm = (0,5,0)
    intercept = (0,0,0)
    length = 5
    prox = (0,0,0)
    G, maxSlab, minSlab = rc.create_network(array, norm, prox)
    branchs = rc.branch_thickness(G)
    for s in branchs:
        for p in branchs[s]:
            print p


def main():
    contents = rc.get_content(rc.data_set)

    # Iterate through data set
    print time.strftime("%d/%m/%Y")
    for subject in rc.subjects[10:]:
        print "Subject: {}".format(subject)
        for muscle in rc.muscles[2:3]:
            #print "Muscle: {}".format(muscle)
            array = contents[subject][0][0][muscle][0][0]['dataPoints'][0][0]

            norm = contents[subject][0][0][muscle][0][0]['nVec'][0][0][0]
            intercept = contents[subject][0][0][muscle][0][0]['endPoint'][0][0][0]
            prox = contents[subject][0][0][muscle][0][0]['endPoint'][0][0][0]
            dist = contents[subject][0][0][muscle][0][0]['endPoint'][0][0][1]
            unitNorm = contents[subject][0][0][muscle][0][0]['norm_nVec'][0][0][0]

            length = rc.magnitude(norm)

            for fat_cutoff in [6.5]:
                print "Fat cutoff: {}".format(fat_cutoff)
                rc.sharp_distinction = fat_cutoff / 100.
                G, maxSlab, minSlab = rc.create_network(array, norm, prox)
                bound = rc.get_boundary(G)

                vis.draw_perp_slab(G, minSlab + 10, norm, intercept, length, unitNorm, bound)
                #ccs = nx.connected_component_subgraphs(G)
                #print "Computing branches"
                #branches = rc.branch_thickness(G)
                #t_values = rc.cc_prox_dist_location(G, norm, prox)
                #for t in t_values:
                    #print "{}, {}: {}".format(t[0],t[2],t[1])
                print maxSlab, minSlab
                """
                with open("results/stcuts/{}.txt".format(subject),'w') as f:
                    for key in branches:
                        for p in branches[key]:
                            f.write(str(p))
                            f.write('\n')
                """

                #branches = rc.branch_thickness_v2(G)

if __name__ == "__main__":
    #cProfile.run('main()')
    main()
    #test()
    #get_axis()
