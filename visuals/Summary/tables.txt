% Fat Voxels
|            Cut off             |    0.05    |   0.055    |    0.06    |   0.065    |    0.07    |   0.075    |    0.08    |   0.085    |    0.09    |   0.095    |    0.1     |
|           Average H            |  0.27779   |  0.24836   |  0.22381   |  0.20341   |  0.18575   |  0.17045   |  0.15684   |  0.14489   |  0.13367   |  0.12384   |  0.11476   |
|           Average T            |   0.4036   |  0.36966   |  0.33947   |  0.31349   |  0.29016   |  0.26948   |   0.251    |  0.23415   |  0.21881   |  0.20454   |  0.19156   |
|            StdDev H            |  0.092257  |  0.085449  |    0.08    |  0.074821  |  0.069831  |  0.065644  |  0.061564  |  0.058162  |  0.054954  |  0.051982  |  0.049129  |
|            StdDev T            |  0.10559   |  0.10201   |  0.096989  |  0.090724  |  0.085092  |  0.08007   |  0.074624  |  0.069512  |  0.064737  |  0.060374  |  0.056678  |

P-values
|                    Cut off                    |    0.05    |   0.055    |    0.06    |   0.065    |    0.07    |   0.075    |    0.08    |   0.085    |    0.09    |   0.095    |    0.1     |
|              # of Fat Components              |  0.25314   |   0.2367   |   0.1572   |   0.1049   |  0.095451  |  0.081968  |  0.066944  |  0.091155  |  0.076424  |  0.059178  |  0.031673  |
|            # of Muscle Components             |  0.40896   |  0.36539   |  0.17913   |  0.18334   |  0.19901   |   0.1178   |  0.084754  |  0.095439  |  0.12026   |  0.061525  |  0.077074  |
|        # of Fat Components, Normalized        |  0.16605   |  0.16586   |  0.081005  |  0.035395  |  0.058415  |  0.049486  |  0.26029   |  0.51553   |  0.58665   |  0.51933   |  0.69304   |
|      # of Muscle Components, Normalized       |  0.16338   |  0.19379   |  0.10105   |  0.092074  |  0.098003  |  0.035521  |  0.024374  |  0.055958  |  0.10908   |  0.064038  |  0.097319  |
|              % of Fat Components              |  0.15973   |  0.20784   |  0.12502   |  0.10907   |  0.13074   |  0.076843  |  0.04766   |  0.081555  |  0.11916   |  0.060863  |  0.072508  |

