import scipy.stats as scs
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
import prettyplotlib as ppl
from prettyplotlib import brewer2mpl
from mpl_toolkits.mplot3d import Axes3D

subjects = [ 'ARC_CN_F01', 'ARC_CN_F03', 'ARC_CN_F04', 'ARC_CN_F05',
    'ARC_CN_F07', 'ARC_CN_M01', 'ARC_CN_M02', 'ARC_CN_M03', 'ARC_CN_M04',
    'ARC_CN_M05', 'ARC_RT_F07', 'ARC_RT_F09', 'ARC_RT_F12', 'ARC_RT_M05',
    'ARC_RT_M06', 'ARC_RT_M07' ]
muscles = ['Infraspinatus','Subscapularis','Supraspinatus','Teres_minor']
healthy_patients = ['ARC_CN_F01', 'ARC_CN_F03', 'ARC_CN_F04', 'ARC_CN_F05',
    'ARC_CN_F07', 'ARC_CN_M01', 'ARC_CN_M02', 'ARC_CN_M03', 'ARC_CN_M04',
    'ARC_CN_M05']
unhealthy_patients = ['ARC_RT_F07', 'ARC_RT_F09', 'ARC_RT_F12', 'ARC_RT_M05',
    'ARC_RT_M06', 'ARC_RT_M07']
sample = ['ARC_CN_F01', 'ARC_CN_M01','ARC_RT_F07','ARC_RT_M05']

sizes = {}
HVoxels = {}
TVoxels = {}
HData = {}
TData = {}
t_tests = {}
t_tests[0] = "# of Fat Components"
t_tests[1] = "# of Muscle Components"
t_tests[2] = "# of Fat Components, Normalized"
t_tests[3] = "# of Muscle Components, Normalized"
t_tests[4] = "% of Fat Components"

cc_sizes = {}
percentages = {}

fat_values = {}

t_values = {}
t_ranges = {}

def load_data():
    global sizes
    global HVoxels
    global TVoxels
    global HData
    global TData
    global cc_sizes
    global percentages
    global fat_values
    global t_values
    global t_ranges

    with open('results/sizes.txt', 'r') as f:
        subject = ""
        for line in f:
            if 'Subject' in line:
                subject = line.split(':')[1].strip()
            elif 'Muscle' in line:
                muscle = line.split(':')[1].strip()
            else:
                sizes[(subject, muscle)] = int(line)

    with open('results/voxels.txt', 'r') as f:
        subject = ""
        for line in f:
            if 'Subject' in line:
                subject = line.split(':')[1].strip()
            else:
                parsed = line.split(',')
                (muscle, fatVoxels, muscleVoxels, total, cut_off) = (parsed[0],
                    float(parsed[1]), float(parsed[2]), float(parsed[3]), float(parsed[4]))

                key = cut_off
                if muscle == muscles[2]:
                    if subject in healthy_patients:
                        if key not in HVoxels:
                            HVoxels[key] = []
                        HVoxels[key].append( fatVoxels / total )
                    else:
                        if key not in TVoxels:
                            TVoxels[key] = []
                        TVoxels[key].append( fatVoxels / total )

    with open('results/brandon.txt','r') as f:
        subject = ""
        for line in f:
            if 'Subject' in line:
                subject = line.split(':')[1].strip()
            else:
                parsed = line.split(',')
                (muscle, fatComponents, muscleComponents, cutoff) = (parsed[0],
                    float(parsed[1]), float(parsed[2]), float(parsed[3]))
                key = (muscle, cutoff)
                total = fatComponents + muscleComponents
                values = (fatComponents, muscleComponents,
                    fatComponents / sizes[(subject, muscle)],
                    muscleComponents / sizes[(subject, muscle)],
                    fatComponents / total)
                if key[0] == muscles[2]:
                    if subject in healthy_patients:
                        if key[1] not in HData:
                            HData[key[1]] = []
                        HData[key[1]].append(values)
                    else:
                        if key[1] not in TData:
                            TData[key[1]] = []
                        TData[key[1]].append(values)

    with open('results/trimmedVerySmall.txt','r') as f:
        subject = ""
        muscle = ""
        cutoff = 0
        fat_sizes = []
        muscle_sizes = []
        for line in f:
            if 'Subject' in line:
                subject = line.split(':')[1].strip()
                cc_sizes[subject] = {}
            elif 'Muscle:' in line:
                muscle = line.split(':')[1].strip()
                cc_sizes[subject][muscle] = {}
            elif 'Fat %' in line:
                cutoff = float(line.split(':')[1].strip())
                cc_sizes[subject][muscle][cutoff] = {}
                cc_sizes[subject][muscle][cutoff]["fat"] = []
                cc_sizes[subject][muscle][cutoff]["muscle"] = []
            elif 'Fat sizes' in line:
                parsed = line.split(':')[1].translate(None, ' \n[]').split(',')
                fat_sizes = map(int, parsed)
                fat_sizes = normalize(fat_sizes, subject, muscle)
                #fat_sizes = np.bincount(fat_sizes)
                #trimmed = np.nonzero(fat_sizes)[0]
                #fat_sizes = zip(trimmed, fat_sizes[trimmed])
                cc_sizes[subject][muscle][cutoff]["fat"].append(fat_sizes)
            elif 'Muscle sizes' in line:
                parsed = line.split(':')[1].translate(None, ' \n[]').split(',')
                muscle_sizes = map(int, parsed)
                muscle_sizes = normalize(muscle_sizes, subject, muscle)
                #muscle_sizes = np.bincount(muscle_sizes)
                #trimmed = np.nonzero(muscle_sizes)[0]
                #muscle_sizes = zip(trimmed, muscle_sizes[trimmed])
                cc_sizes[subject][muscle][cutoff]["muscle"].append(muscle_sizes)

    with open('results/ones.txt','r') as f:
        subject = ""
        cutoff = 0
        percentages = {}
        for line in f:
            if 'Subject' in line:
                subject = line.split(':')[1].strip()
                percentages[subject] = []
            else:
                parsed = line.split(",")
                cutoff = float(parsed[0])
                total, onBoundary = float(parsed[1]), float(parsed[2])
                percentages[subject].append(onBoundary/total)

    with open('results/fatsAll.txt','r') as f:
        subject = ""
        fat_values= {}
        for line in f:
            if 'Subject' in line:
                subject = line.split(':')[1].strip()
                fat_values[subject] = []
            elif "/" not in line:
                fat = float(line) * 100.0
                fat_values[subject].append(fat)

    with open('results/t_values.txt','r') as f:
        subject = ""
        t_values = {}
        cut_off = 0.0
        for line in f:
            if 'Subject' in line:
                subject = line.split(':')[1].strip()
                t_values[subject] = {} 
            elif 'Fat cutoff' in line:
                cut_off = float(line.split(':')[1].strip())
                t_values[subject][cut_off] = [] 
            elif '/' not in line:
                key, t_val = line.split(':')
                weight, size = key.split(',')
                weight = float(weight.strip())
                size = float(size.strip())
                t_val = float(t_val.strip())
                key = (weight, size)
                t_values[subject][cut_off].append((t_val, weight, size))

    with open('results/t_ranges.txt','r') as f:
        subject = ""
        t_ranges= {}
        cut_off = 0.0
        for line in f:
            if 'Subject' in line:
                subject = line.split(':')[1].strip()
            elif 'Fat cutoff' in line:
                cut_off = float(line.split(':')[1].strip())
            elif '/' not in line:
                max_t, min_t = line.split()
                max_t = float(max_t)
                min_t = float(min_t)
                t_ranges[subject] = (max_t, min_t)

def percent_voxels():
    global HVoxels
    global TVoxels

    row = "| {:^30} |" + " {:^10.5} |" * len(HVoxels)
    print row.format("Cut off", *sorted(HVoxels))
    AvgH = [ sum(HVoxels[i]) / len(HVoxels[i]) for i in sorted(HVoxels) ]
    AvgT = [ sum(TVoxels[i]) / len(TVoxels[i]) for i in sorted(TVoxels) ]
    StdH = [ scs.tstd(HVoxels[i]) for i in sorted(HVoxels) ]
    StdT = [ scs.tstd(TVoxels[i]) for i in sorted(TVoxels) ]
    print row.format("Average H", *AvgH)
    print row.format("Average T", *AvgT)
    print row.format("StdDev H", *StdH)
    print row.format("StdDev T", *StdT)
    print


def parse_colormap():
    seq = []
    NUM_COLORS = 256.0
    index = 0.0
    with open('colormap.txt') as f:
        for line in f:
            rgb = [float(l) for l in line.split()]
            seq.append(rgb)
            seq.append(index / NUM_COLORS)
            index += 1.0
    return seq[:-5]

def make_colormap(seq):
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        print i, item
        if isinstance(item, float):
            print seq[i+1]
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return mcolors.LinearSegmentedColormap('CustomMap', cdict)


def diverge_map(high=(0.565, 0.392, 0.173), low=(0.094, 0.310, 0.635)):
    '''
    low and high are colors that will be used for the two
    ends of the spectrum. they can be either color strings
    or rgb color tuples
    '''
    c = mcolors.ColorConverter().to_rgb
    if isinstance(low, basestring): low = c(low)
    if isinstance(high, basestring): high = c(high)
    return make_colormap([low, c('white'), 0.5, c('white'), high])

def t_test_heatmap():
    global HData
    global TData
    global t_tests

    row_labels = []
    for key in sorted(HData):
        row_labels.append("{} %".format(key * 100))

    data = []
    row = "| {:^45} |" + " {:^10.5} |" * len(HData)
    print row.format("Cut off", *row_labels)
    for value in sorted(t_tests):
        results = []
        heat = []
        for key in sorted(HData):
            statH = []
            statT = []
            for i in range(len(HData[key])):
                statH.append(HData[key][i][value])
            for i in range(len(TData[key])):
                statT.append(TData[key][i][value])
            print key, np.mean(statH), np.mean(statT)
            pvalue = scs.ttest_ind(statH, statT, equal_var=False)[1]

            """
            if pvalue <= 0.25:
                pvalue *= 0.4 / 0.25
                pvalue -= .04
            else:
                pvalue = (pvalue - 0.25) * (2.0/3.0) + 0.5
            """

            results.append(pvalue)

        print row.format(t_tests[value], *results)
        data.append(results)

    data = np.array(data)
    x_labels = row_labels[:]
    y_labels = ["# Fat","# Muscle", "# Fat Normalized", "# Muscle Normalized", "% Fat"]

    fig, ax = plt.subplots(1)

    """
    colormap = brewer2mpl.get_map('Purples', 'sequential', 9).mpl_colormap
    ppl.pcolormesh(fig, ax, data,
        xticklabels=x_labels, yticklabels=y_labels,
        cmap=colormap, center_value=2)
    """
    #heatmap = ax.pcolor(data, cmap=plt.cm.Reds_r, edgecolors='k')
    heatmap = ax.pcolor(data, cmap=plt.cm.RdYlGn_r, edgecolors='k')
    #heatmap = ax.pcolor(data, cmap=make_colormap(parse_colormap()), edgecolors='k')

    # put the major ticks at the middle of each cell
    ax.set_xticks(np.arange(len(x_labels))+0.5, minor=False)
    ax.set_yticks(np.arange(len(y_labels))+0.5, minor=False)

    ax.set_xticklabels(x_labels)
    ax.set_yticklabels(y_labels)

    # want a more natural, table-like display
    ax.xaxis.tick_top()

    ax.set_ylim((0,5))
    ax.set_xlim((0,11))
    ax.set_xlabel("Cut off %")
    ax.xaxis.set_label_position('top')
    ax.set_ylabel("Statistic Metric")
    ax.xaxis.label.set_fontsize(24)
    ax.yaxis.label.set_fontsize(24)
    ax.tick_params(axis='both', which='major', labelsize=13)

    cb = fig.colorbar(heatmap)
    cb.ax.set_xlabel("P-value")
    cb.ax.xaxis.set_label_position('top')
    cb.ax.xaxis.label.set_fontsize(24)
    #cb.ax.set_xticks([0,.25,.5,.75,1])
    #cb.ax.set_xtickslabels([0,0.1,0.25,0.5,1.0])

    #fig.savefig('test.png')
    plt.show()

def p_vectors():
    bin1 = 0.01
    bin2 = 1
    bin3 = 100

    hpvec = [0,0,0]
    tpvec = [0,0,0]

    for subject in subjects:
        for muscle in muscles[2:3]:
            for cutoff in [6.5]:
                ccs = cc_sizes[subject][muscle][cutoff]
                for fat in ccs["fat"][0]:
                    if fat >= 0 and fat < bin1:
                        if subject in healthy_patients:
                            hpvec[0] += 1
                        else:
                            tpvec[0] += 1
                    elif fat >= bin1 and fat < bin2:
                        if subject in healthy_patients:
                            hpvec[1] += 1
                        else:
                            tpvec[1] += 1
                    elif fat >= bin2 and fat < bin3:
                        if subject in healthy_patients:
                            hpvec[2] += 1
                        else:
                            tpvec[2] += 1

    htotal = sum(hpvec)
    ttotal = sum(tpvec)
    for i in range(3):
        hpvec[i] /= float(htotal)
        tpvec[i] /= float(ttotal)
        #hpvec[i] *= ttotal 
        #tpvec[i] *= 100

    print hpvec, tpvec
    print "Chi square and P-value: {}".format(scs.chisquare(tpvec, f_exp=hpvec))

    """
    fig = plt.figure()
    width = 0.25
    ticks = [0.15,0.45,0.75]
    ax = fig.add_subplot(111)
    """

    hx = []
    hy = []
    hz = []
    tx = []
    ty = []
    tz = []

    hsizes = []
    tsizes = []
    for subject in subjects:
        for muscle in muscles[2:3]:
            for cutoff in [6.5]:
                ccs = cc_sizes[subject][muscle][cutoff]
                pvec = [0,0,0]
                for fat in ccs["fat"][0]:
                    if fat >= 0 and fat < bin1:
                        pvec[0] += 1
                    elif fat >= bin1 and fat < bin2:
                        pvec[1] += 1
                    elif fat >= bin2 and fat < bin3:
                        if subject in healthy_patients:
                            hsizes.append(fat)
                        else:
                            tsizes.append(fat)
                        pvec[2] += 1
                total = sum(pvec)
                for i in range(3):
                    pvec[i] /= float(total)
                    #pvec[i] *= 100

                if subject in healthy_patients:
                    hx.append(pvec[0])
                    hy.append(pvec[1])
                    hz.append(pvec[2])
                elif subject in unhealthy_patients:
                    tx.append(pvec[0])
                    ty.append(pvec[1])
                    tz.append(pvec[2])

    print np.mean(hsizes), np.mean(tsizes)

    print scs.ttest_ind(hsizes, tsizes, equal_var=False)[1]

    hfunc = []
    tfunc = []
    for i in range(len(hx)):
        val = hx[i] + hy[i] * 5 + hz[i] * 25
        hfunc.append(val)

    for i in range(len(tx)):
        val = tx[i] + ty[i] * 5 + tz[i] * 25
        tfunc.append(val)

    x_pvalue = scs.ttest_ind(hx, tx, equal_var=False)[1]
    y_pvalue = scs.ttest_ind(hy, ty, equal_var=False)[1]
    z_pvalue = scs.ttest_ind(hz, tz, equal_var=False)[1]
    f_pvalue = scs.ttest_ind(hfunc, tfunc, equal_var=False)[1]
    #print x_pvalue, y_pvalue, z_pvalue
    #print f_pvalue

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(hx,hy,hz,c='b')
    ax.scatter(tx,ty,tz,c='r')
    """
    plt.title("Healthy Patients: Fat Component Size Distribution P-Vectors(Cut off: {})".format(cutoff))
    plt.xlabel("Component Size (% of total muscle size)")
    plt.ylabel("Probability")
    plt.ylim([0,100])
    plt.xticks(ticks)
    ax.set_xticklabels(["Very small","small","large"])
    """
    plt.show()

def cc():
    bin1 = 0.01
    bin2 = 1
    bin3 = 50

    width = 0.25
    ticks = [0.15,0.45,0.75]
    pbins = []
    pbins.append([])
    pbins.append([])
    pbins.append([])
    colors = ['b','g','r','c','m','y','k']
    colorIndex = 0

    pTotal = []
    pTotal.append([])
    pTotal.append([])
    pTotal.append([])

    totalCounts = [0,0,0]
    for subject in healthy_patients:
        print "Subject: {}".format(subject)
        for muscle in muscles[2:3]:
            print "Muscle: {}".format(muscle)
            for cutoff in [6.5]:
                print "Cutoff: {}".format(cutoff)
                ccs = cc_sizes[subject][muscle][cutoff]
                pbins[0] = []
                pbins[1] = []
                pbins[2] = []
                ccBin = []
                if subject in healthy_patients:
                    for fat in ccs["fat"][0]:
                        if fat >= 0 and fat < bin1:
                            pbins[0].append(fat)
                            pTotal[0].append(fat)
                        elif fat >= bin1 and fat < bin2:
                            pbins[1].append(fat)
                            pTotal[1].append(fat)
                        elif fat >= bin2 and fat < bin3:
                            pbins[2].append(fat)
                            pTotal[2].append(fat)
                    total = 0
                    pvec = []
                    for b in pbins:
                        total += len(b)
                    for b in pbins:
                        pvec.append(len(b) / float(total))
                    totalCounts[0] += len(pbins[0])
                    totalCounts[1] += len(pbins[1])
                    totalCounts[2] += len(pbins[2])


                    #P Vectors
                    #fig = plt.figure()
                    #ax = fig.add_subplot(111)
                    #plt.bar(ticks,pvec,width,fill=False,edgecolor=colors[colorIndex],align='center',linewidth=2)
                    #ax.bar(ticks,pvec,width,fill=True,edgecolor=colors[colorIndex],color=colors[colorIndex],align='center',linewidth=2)
                    #ax.bar(ticks,pvec,width,fill=True,edgecolor=colors[colorIndex],color=colors[colorIndex],align='center',linewidth=2)
                    #fig.suptitle("P Vector for Subject: {}, Cutoff: {}".format(subject, cutoff))
                    #plt.xlabel("Component Size (% of total muscle size)")
                    #plt.ylabel("Probability")
                    #plt.xticks(ticks)
                    #ax.set_xticklabels(["Very small","small","large"])
                    #ax.set_ylim(0,1.0)
                    #ax.set_xlim(0,50)

                    # Component sizes
                    """
                    fig = plt.figure()
                    ax = fig.add_subplot(111)
                    ax.hist(ccs["fat"][0], bins=range(0,10))
                    fig.suptitle("Component Sizes for Subject: {}, Cutoff: {}".format(subject, cutoff))
                    plt.ylabel("Number in bin")
                    plt.xlabel("CC Sizes (in % of total muscle volume)")
                    ax.set_xlim(0,10)
                    ax.set_ylim(0,60)
                    """

            colorIndex += 1
            if colorIndex >= 7:
                colorIndex = 0

    total = 0
    for b in pTotal:
        total += len(b)
    pvec = []
    for b in pTotal:
        pvec.append(len(b) / float(total))

    print pvec
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #plt.title("Healthy Patients: Fat Component Size Distribution P-Vectors(Cut off: {})".format(cutoff))
    plt.title("Tear Patients: Fat Component Size Distribution P-Vectors(Cut off: {})".format(cutoff))
    ax.bar(ticks,pvec,width,fill=True,edgecolor='m',color='m',align='center',linewidth=2)
    plt.xlabel("Component Size (% of total muscle size)")
    plt.ylabel("Probability")
    plt.xticks(ticks)
    ax.set_xticklabels(["Very small","small","large"])

    totalC = sum(totalCounts)
    totalCounts = [ i / float(totalC) * 100.0 for i in totalCounts ]

    # Total counts
    #fig = plt.figure()
    #ax = fig.add_subplot(111)
    #plt.bar(ticks,totalCounts,width,fill=False,edgecolor=colors[colorIndex],align='center',linewidth=2)
    #fig.suptitle("Total Counts For Unhealthy, Cutoff: {}".format(cutoff))
    #plt.ylabel("% of Total Counts")
    #plt.xticks(ticks)
    #ax.set_xticklabels(["Very small","small","large"])

    plt.show()

def normalize( collection, subject, muscle):
    normalized = []
    for col in collection:
        normalized.append( 100 * float(col)/sizes[(subject,muscle)] )
    return normalized

def fat_hist():
    global fat_values
    colors = ['b','g','r','c','y','m','k']
    colorIndex = 0
    fig = plt.figure()
    ax = fig.add_subplot(111)
    fats = []
    for subject in healthy_patients:
        fats.extend(fat_values[subject])
        # Component sizes
        #fig = plt.figure()
        #ax = fig.add_subplot(111)
        #x,y,o = ax.hist(fat_values[subject], bins=range(0,100),color=colors[colorIndex])
        #x,y,o = ax.hist(fat_values[subject], bins=range(0,100),color=colors[colorIndex],fill=False, edgecolor=colors[colorIndex],normed=True)
        #fig.suptitle("Fat %'s of Voxels for Subject: {}".format(subject))
        #plt.ylabel("Counts")
        #plt.xlabel("Fat %'s")
        #ax.set_xlim(0,100)
        #ax.set_ylim(0,23000)

        colorIndex += 1
        if colorIndex >= 6:
            colorIndex = 0
        #print "Subject: {}, max: {} {}".format(subject,x.max(),y[x.argmax()])
    x,y,o = ax.hist(fats, bins=range(0,100),color=colors[colorIndex],fill=False, edgecolor=colors[colorIndex],normed=True)
    fig.suptitle("Fat %'s of Voxels for Healthy Patients")
    plt.ylabel("% of Voxels")
    plt.xlabel("Fat %'s")
    ax.set_xlim(0,100)
    ax.set_ylim(0,0.4)
    plt.show()
    return

def setBoxColors(bp):
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['fliers'][1], color='blue')
    plt.setp(bp['medians'][0], color='blue')

    plt.setp(bp['boxes'][1], color='red')
    plt.setp(bp['caps'][2], color='red')
    plt.setp(bp['caps'][3], color='red')
    plt.setp(bp['whiskers'][2], color='red')
    plt.setp(bp['whiskers'][3], color='red')
    plt.setp(bp['fliers'][2], color='red')
    plt.setp(bp['fliers'][3], color='red')
    plt.setp(bp['medians'][1], color='red')

def prox_distal():
    bin_lims = [25,75,100]#[20,40,60,80,100]
    H = [ [] for b in bin_lims ]
    T = [ [] for b in bin_lims ]
    Hvs = [ [] for b in bin_lims ]
    Tvs = [ [] for b in bin_lims ]
    Hs = [ [] for b in bin_lims ]
    Ts = [ [] for b in bin_lims ]
    FullH = []
    FullT = []
    weights = [0.01, 1, 100]
    for subject in t_values:
        max_t, min_t = t_ranges[subject]
        full = max_t - min_t
        VS = 0
        S = 0
        L = 0
        for cutoff in [6.5]:
            #print "Subject: {}, Cut off: {}".format(subject, cutoff)
            t_values[subject][cutoff].sort()
            bins = [ [] for b in bin_lims ]
            vs_counts = [ 0 for b in bin_lims ]
            s_counts = [ 0 for b in bin_lims ]

            for t_value, weight, size in t_values[subject][cutoff]:
                normed = (t_value - min_t) / full * 100.0
                for i in range(len(bin_lims)):
                    if normed < bin_lims[i]:
                        bins[i].append(normed)
                        if weight == weights[0]:
                            vs_counts[i] += 1
                        elif weight == weights[1]:
                            s_counts[i] += 1
                        break

                if weight == weights[0]:
                    VS += 1
                elif weight == weights[1]:
                    S += 1
                else:
                    L += 1

            for i in range(len(bins)):
                if subject in healthy_patients:
                    Hvs[i].append(vs_counts[i] * 100. / VS)
                    Hs[i].append(s_counts[i] * 100. / S)
                    H[i].append(len(bins[i]) * 100. / (VS + S + L))
                else:
                    Tvs[i].append(vs_counts[i] * 100. / VS)
                    Ts[i].append(s_counts[i] * 100. / S)
                    T[i].append(len(bins[i]) * 100. / (VS + S + L))

    #T tests
    p_values = [ [] for b in bins ]
    p_vec = []
    for h, t in zip(H, T):
        p_vec.append(scs.ttest_ind(h,t,equal_var=False)[1])
    p_values[0].extend(p_vec)

    p_vec = []
    for h, t in zip(Hvs, Tvs):
        p_vec.append(scs.ttest_ind(h,t,equal_var=False)[1])
    p_values[1].extend(p_vec)

    p_vec = []
    for h, t in zip(Hs, Ts): 
        p_vec.append(scs.ttest_ind(h,t,equal_var=False)[1])
    p_values[2].extend(p_vec)

    row = " {:.4f} |" * len(bins)

    print "      |"+(" {:.3f} |" * (len(bins) - 1) + " {:.2f} |").format(*bin_lims)
    print "total |"+row.format(*p_values[0])
    print "vs    |"+row.format(*p_values[1])
    print "s     |"+row.format(*p_values[2])

    fig = plt.figure()
    ax = plt.axes()
    boxes = []
    for i in range(len(Hvs)):
        h = Hvs[i]
        t = Tvs[i]
        boxes.append([h,t])
    for i in range(len(boxes)):
        bp = plt.boxplot(boxes[i], positions=[1 + 3 * i, 2 + 3 * i], widths=0.6)
        setBoxColors(bp)
    plt.xlim(0, 3 * len(boxes))
    axis_font = {'fontname':'Arial', 'size':'20'}
    plt.title("Very Small (< 0.01% volume) Components", **axis_font)
    plt.ylabel("% of components", **axis_font)
    plt.xlabel("Location along proximal-distal line", **axis_font)
    plt.ylim(0,100)

    ax.set_xticklabels(["","Proximal 25%","Middle 50%","Distal 25%", ""])
    maxtick = 3 * len(boxes)
    ax.set_xticks([0,maxtick * .15, maxtick * .5, maxtick * .85, maxtick])
    ax.tick_params(axis='both', which='major', labelsize=15)
    #plt.savefig("visuals/box/5Sections/VS{}.png".format(bin_lims[i]),bbox_inches='tight')
    plt.show()

    fig = plt.figure()
    ax = plt.axes()
    boxes = []
    for i in range(len(H)):
        h = H[i]
        t = T[i]
        boxes.append([h,t])
        #plt.boxplot([h,t])
    for i in range(len(boxes)):
        bp = plt.boxplot(boxes[i], positions=[1 + 3 * i, 2 + 3 * i], widths=0.6)
        setBoxColors(bp)
    plt.xlim(0, 3 * len(boxes))
    #plt.title("VS components, Upper limit: {}".format(bin_lims[i]))
    axis_font = {'fontname':'Arial', 'size':'20'}
    plt.title("All Components", **axis_font)
    plt.ylabel("% of components", **axis_font)
    plt.xlabel("Location along proximal-distal line", **axis_font)
    plt.ylim(0,100)

    ax.set_xticklabels(["","Proximal 25%","Middle 50%","Distal 25%", ""])
    maxtick = 3 * len(boxes)
    ax.set_xticks([0,maxtick * .15, maxtick * .5, maxtick * .85, maxtick])
    ax.tick_params(axis='both', which='major', labelsize=15)
    #plt.savefig("visuals/box/5Sections/VS{}.png".format(bin_lims[i]),bbox_inches='tight')
    plt.show()

    '''
    fig, ax = plt.subplots(1)
    colors = [(0,0,0), (1,0,0), (0,1,0), (0,0,1), (1,1,0)]
    controls = []
    count = 0
    oldBar = [0 for i in range(17)]
    ticks = [i * 15 + 5 for i in range(17)]

    C = H[:]
    for i in range(len(C)):
        C[i].append(0)
        C[i].extend(T[i])

    for i in range(len(C)):
        print len(C[i])
        ax.bar(ticks, C[i], 10, bottom=oldBar, color=colors[i])
        for j in range(len(oldBar)):
            oldBar[j] += C[i][j]

    #ax.bar(bin_lims, bar, 10, edgecolor=colors[i], bottom=oldBar)
    plt.ylabel("% of components")
    plt.title("Bin distributions")
    ax.set_xlim((0,260))
    ax.set_ylim((0,101))
    plt.show()
    '''


#main

load_data()
#for patient in sorted(percentages):
    #print "{}: {}".format(patient,percentages[patient][0])
#print cc_sizes
#cc()
#fat_hist()
#t_test_heatmap()
#percent_voxels()
#p_vectors()
prox_distal()
