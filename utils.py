import math
import numpy as np

def dot_prod( vec1, vec2 ):
    value = sum(vec1[i] * vec2[i] for i in range(len(vec1)))
    return value


def magnitude( vec ):
    mag = math.sqrt(sum(i*i for i in vec))
    return mag


def cross_prod( vec1, vec2 ):
    return np.array([vec1[1] * vec2[2] - vec1[2] * vec2[1],
        vec1[2] * vec2[0] - vec1[0] * vec2[2],
        vec1[0] * vec2[1] - vec1[1] * vec2[0]])


def projection( source, destUnitNorm ):
    prod = np.dot(source, destUnitNorm)
    proj = source - prod * destUnitNorm
    return proj


def vector_projection( source, dest ):
    mag = magnitude( dest )
    unitDest = dest / magnitude(dest)
    return np.dot( source, unitDest ) * unitDest
