# Rotator Cuff Research Project
#
# Author(s): Brandon Mork, Yang Ho
#   Created: Spring 2015
#
#     Input: .mat file containing:
#            | fatFraction | x | y | z |

import sys, os

data_set = 'Rotator_cuff_tear_study_image_data_for_dissemination.mat'

# Data sets to look at
subjects = [ 'ARC_CN_F01', 'ARC_CN_F03', 'ARC_CN_F04', 'ARC_CN_F05',
    'ARC_CN_F07', 'ARC_CN_M01', 'ARC_CN_M02', 'ARC_CN_M03', 'ARC_CN_M04',
    'ARC_CN_M05', 'ARC_RT_F07', 'ARC_RT_F09', 'ARC_RT_F12', 'ARC_RT_M05',
    'ARC_RT_M06', 'ARC_RT_M07' ]
muscles = ['Infraspinatus','Subscapularis','Supraspinatus','Teres_minor']
fat_range = [5.0 + x * 0.5 for x in range(11)]

slab_width = 1.0000

# Network creation options
node_adjacency = 'Cube'      #Cube or Coordinate
edge_creation = 'Sharp'      #Sharp or Gradient
sharp_distinction = .1       #Float ~0.100
gradient_distinction = .025  #Float ~0.025

visualization = '2D'

maximum = 0
minimum = 100

# Dependancies
import scipy.io as sio
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import math

# 3-D visualization
from mpl_toolkits.mplot3d import Axes3D

# S-T Cutset
from networkx.algorithms.connectivity import minimum_st_node_cut

# Custom modules
from utils import *

# Functions
# Node related
def get_location_adjacent_nodes( node ):
    """
    Returns a tuple of all location-adjacent nodes.
    Uses node_adjacency option from top of script.

    :param node: type str: coordinate string 'xxxyyyzzz'

    if adjacency == 'Cube'
    :return: type tuple of str: tuple of 26 names of nodes in a cube around node

    if adjacency == 'Coordinate'
    :return: type tuple of str: tuple of 8 names of nodes 
      horizontally/vertically next to node

    Author: Brandon Mork
    """

    global node_adjacency
    x, y, z = node
    if node_adjacency == 'Cube':
        dis = [-1, 0, 1]
        res = [(x+i, y+j, z+k) for i in dis for j in dis for k in dis]
        res.remove(node)
        return res
    elif node_adjacency == 'Coordinate':
        dis = [-1, 1]
        res = [(x+i, y, z) for i in dis]
        res += [(x , y+i, z) for i in dis]
        return res + [(x, y, z+i) for i in dis]
    else:
        sys.stderr.write( 'ERROR: Value of "node_adjacency" option at the top of the script is invalid.\n' )
        sys.exit(1)


def node_size( G, node ):
    """
    Returns a float 1 to 2 indicating node's connectivity to adjacent slices.
    Uses node_adjacency option from top of script.

    :param G: type nx graph: network x graph representing muscle
    :param node: type str: coordinate string 'xxxyyyzzz'

    :return: type float: number (1 to 2) representing edge-adjacent nodes in 
      slices y+1 and y-1

    Author: Brandon Mork
    """

    num_adjacent = 0.0
    for neighbor in G.neighbors(node):
        if neighbor[1] != node[1]:  # If not in the same y_slice
            num_adjacent += 1
    global node_adjacency
    if node_adjacency == 'Cube':
        return (num_adjacent + 18) / 18
    elif node_adjacency == 'Coordinate':
        return (num_adjacent + 2) / 2
    else:
        sys.stderr.write('ERROR: Value of "node_adjacency" option at the top of the script is invalid.\n')
        sys.exit(1)


# Graph related
def create_network( array, norm, prox ):
    """
    Returns nx graph representing muscle array.
    Uses edge_creation, sharp_distinction, gradient_distinction options from 
      top of script.

    if edge_creation == 'Sharp':
    if edge_creation == 'Gradient':

    :param: type np array: Numpy array formatted like: 
      | fatFraction | x | y | z |

    :return: Tuple: (NetworkX graph representing array, maximum slab number,
        minimum slab number)

    Author: Brandon Mork
    """

    global edge_creation
    global sharp_distinction

    G = nx.Graph()

    length = magnitude(norm)

    max_slab, min_slab = 0, int(length)

    for fat, x, y, z in array:
        node = (x,y,z)
        
        # Compute t value of node
        d = np.dot(norm, node)
        n_dot_o = np.dot(norm, prox)
        n_dot_t = np.dot(norm, norm)
        t = (d - n_dot_o) / n_dot_t

        #node_slab = t
        node_slab = int(t * length)

        if node_slab < min_slab:
            min_slab = node_slab
        if node_slab > max_slab:
            max_slab = node_slab


        node_type = True if (fat >= sharp_distinction and edge_creation == 'Sharp') else False

        G.add_node((x, y, z), fatFraction=fat, isFat=node_type , slab=node_slab)

    nodes = G.nodes()
    if edge_creation == 'Sharp':
        for node in nodes:
            node_fat = G.node[node]['fatFraction']
            neighbors = get_location_adjacent_nodes(node)
            border = False
            for neighbor in neighbors:
                if neighbor in G:
                    neighbor_fat = G.node[neighbor]['fatFraction']
                    if node_fat <= sharp_distinction and neighbor_fat <= sharp_distinction:
                        G.add_edge(node, neighbor)
                    elif node_fat > sharp_distinction and neighbor_fat > sharp_distinction:
                        G.add_edge(node, neighbor)
                    else:
                        border = True
            G.node[node]['border'] = border
    elif edge_creation == 'Gradient':
        global gradient_distinction
        global maximum
        global minimum
        for node in nodes:
            node_fat = G.node[node]['fatFraction']
            neighbors = get_location_adjacent_nodes(node)
            for neighbor in neighbors:
                if neighbor in G and (abs(node_fat - G.node[neighbor]['fatFraction']) < gradient_distinction):
                    G.add_edge(node, neighbor)

            if node_fat > maximum:
                maximum = node_fat
            elif node_fat < minimum:
                minimum = node_fat

    ccs = nx.connected_components(G)
    cc_index = 0
    for cc in ccs:
        for n in cc:
            G.node[n]['Component'] = cc_index
        cc_index += 1

    return G


def get_boundary( G ):
    """
    Returns a list of all nodes on the boundary of the given graph.
    The boundary is defined as the outermost nodes that have neighboring 
      locations that are not in G.

    Differences between boundary and border:
      boundary - contains the entire muscle mass
      border - seperates nodes that are not connected to one another
        i.e. seperates fat and muscle components

    :param G: type nx graph: network x graph representing muscle
    
    :return: bound: type list: list containing all nodes on the boundary

    Author: Yang Ho
    """

    bound = [] 
    for node in G:
        neighbors = get_location_adjacent_nodes(node)
        for neighbor in neighbors:
            if neighbor not in G:
                bound.append(node)
                break
    return bound


def compute_diameters( G ):
    """
    Returns the diameters for each connected components for a given graph.

    :param G: type nx graph: network x graph representing muscle

    :return: type tuple of list: (diameters for fat components, diameters for
      muscle components)

    Author: Yang Ho 
    """ 
    f_diam = []
    m_diam = []
    ccs = nx.connected_component_subgraphs(G)
    for cc in ccs:
        diam = nx.diameter(cc)
        if cc.nodes()[0]['isFat']:
            f_diam.append(nx.diameter(cc))
        else:
            m_diam.append(nx.diameter(cc))
    return f_diam, m_diam


def count_connected_components( G ):
    """
    Returns number of fat CC and muscle CC in the sharp distinction network.
    Uses edge_creation and sharp_distinction from top of script.
    Function requires edge_creation to be set to 'Sharp'.

    :param G: type nx Graph: NetworkX graph representing muscle

    :return: type tuple of int: ( # fat CC, # muscle CC, # total CC, 
      # of fat voxels, # of muscle voxels, fat CC sizes, muscle CC sizes)

    Author: Brandon Mork
    """

    global edge_creation
    if edge_creation != 'Sharp':
        sys.stderr.write('ERROR: Value of "edge_creation" option at top of script must be "Sharp" to count components.\n')
        sys.exit(1)

    global sharp_distinction
    fat, mus = 0, 0
    fat_vox, mus_vox = 0, 0
    f_sizes = []
    m_sizes = []
    ccs = nx.connected_components(G)
    for cc in ccs:
        if G.node[cc[0]]['isFat']:
            f_sizes.append(len(cc))
            fat += 1
            fat_vox += len(cc)
        else:
            m_sizes.append(len(cc))
            mus += 1
            mus_vox += len(cc)
    return fat, mus, fat + mus, fat_vox, mus_vox, f_sizes, m_sizes


def cc_prox_dist_location( G, norm, prox ):
    """
    Computes the approximated center location of the connected components along
    the prox-distal line. Projects all voxels in a component onto the prox-distal
    line and uses the median t-value as the components location.

    :param G: type nx Graph: NetworkX graph representing muscle
    :param norm: type np.array: normal vector of prox-distal line
    :param prox: type np.array: proximal position vector

    :return: type list of center t-values

    Author: Yang Ho 
    """

    ccs = nx.connected_components(G)
    volume = float(G.number_of_nodes())
    center_t_values = []
    for cc in ccs:
        percent = 100.0 * len(cc) / volume
        if percent <= 0.01:
            size = .01 
        elif percent <= 1:
            size = 1
        else:
            size = 100

        t_values = [] 
        isFat = False
        for node in cc:
            if G.node[node]['isFat']:
                isFat = True
            else:
                break

            if isFat:
                # Compute t value of node
                d = np.dot(norm, node)
                n_dot_o = np.dot(norm, prox)
                n_dot_t = np.dot(norm, norm)
                t = (d - n_dot_o) / n_dot_t

                t_values.append(t)

        if isFat:
            center_t = np.mean(t_values) 
            center_t_values.append((size, center_t, percent))

    return center_t_values


def trim_graph( G, boundary ):
    """
    Trims the very small fat components that are completely contained in the 
      boundary.

    :param G: type nx Graph: NetworkX graph representing muscle
    :param boundary: type list: list of all boundary nodes

    :return : type nx Graph: the trimmed graph

    Author: Yang Ho
    """
    boundary = get_boundary(G)
    ccs = nx.connected_components(G)
    to_remove = []
    volume = float(G.number_of_nodes())
    for cc in ccs:
        if G.node[cc[0]]['isFat']:
            percent = 100.0 * len(cc) / volume
            if percent <= 0.01:
                if all(c in boundary for c in cc):
                    to_remove.extend(cc)
    G.remove_nodes_from(toRemove)
    return G


def cc_on_boundary( G, boundary ):
    """
    computes the number of fat components contained on the boundary

    :param G: type nx Graph: NetworkX graph representing muscle
    :param boundary: type list: list of all boundary nodes

    :return : type tuple of int: total # of fat components, # on boundary
    Author: Yang Ho
    """
    ccs = nx.connected_components(G)
    total = 0
    on_bound = 0
    for cc in ccs:
        if G.node[cc[0]]['isFat']:
            total += 1
            if all(c in boundary for c in cc):
                on_bound += 1

    return total, on_bound


def prox_dist_path( G, norm, prox, dist, bound ):
    """
    Determines if there is a path from the proximal points to the distal points.
    
    :param G: type nx Graph: NetworkX graph representing muscle
    :param norm: type np.array: normal vector of prox-distal line
    :param prox: type np.array: proximal position vector
    :param dist: type np.array: distal position vector
    :param bound: type list: list of boundary nodes

    :return : type boolean

    Author: Yang Ho
    """
    prox_end = tuple(prox[:])
    p_ends = []
    dist_end = tuple(dist[:])
    d_ends = []
    prox_proj = 10
    dist_proj = -10
    for n in bound.nodes():
        if not G.node[n]['isFat']:
            v_proj = vector_projection( np.array(n)- prox, norm)
            diff = v_proj - prox
            t = diff[0] / norm[0]
            if t < prox_proj:
                prox_proj = t
                prox_end = n[:]
                p_ends.append(prox_end)
            elif (prox_proj - 0.005 <= t <= prox_proj + 0.005):
                p_ends.append(n[:])
            if t > dist_proj:
                dist_proj = t
                dist_end = n[:]
                d_ends.append(prox_end)
            elif (dist_proj - 0.005 <= t <= dist_proj + 0.005):
                d_ends.append(n[:])

    return all([nx.has_path(G,p,d) for p in p_ends for d in dEnds])


"""
Not working well, terribly inefficient.
"""
def branch_thickness( G ):
    """
    Computes branch thickness
    :param
    :return
    Author (s): Yang Ho
    """ 
    # Get slabs
    slabs = {}
    for node in G:
        if not G.node[node]['isFat']:
            node_slab = G.node[node]['slab']
            if node_slab not in slabs:
                slabs[node_slab] = []
            slabs[node_slab].append(node)

    # Get the branch components
    components = nx.DiGraph()
    current_comps = []
    comp_index = 0
    for slab in slabs:
        slab_comps = nx.connected_components(G.subgraph(slabs[slab]))
        new_comps = []
        for scc in slab_comps:
            adj_comps = []
            neighbors = set()
            for n in scc:
                adj_nodes = get_location_adjacent_nodes(n)
                for adj in adj_nodes:
                    if adj in G:
                        if not G.node[adj]['isFat']:
                            neighbors.add(adj)
            for comp in current_comps:
                if any([n in components.node[comp]['nodes'] for n in neighbors]):
                    adj_comps.append(comp)
            count = len(adj_comps)

            components.add_node(comp_index, nodes=scc)
            for adj in adj_comps:
                components.add_edge(adj, comp_index)
            new_comps.append(comp_index)
            comp_index += 1
        current_comps = new_comps[:] 
    
    # Get path end points, eccen., and subgraphs
    subgraphs = {}
    starts = []
    ends = []
    critical = []
    for comp in components:
        predecessors = components.predecessors(comp)
        successors = components.successors(comp)
        pred_len = len(predecessors)
        succ_len = len(successors)
        if pred_len == 0:
            starts.append(comp)
        if succ_len == 0:
            ends.append(comp)

        subgraphs[comp] = G.subgraph(components.node[comp]['nodes'])
        if pred_len > 0 and succ_len > 0:
            subgraphs[comp].add_node('s')
            subgraphs[comp].add_node('t')
            s_adj = []
            t_adj = []
            for n in components.node[comp]['nodes']:
                adj_nodes = get_location_adjacent_nodes(n)
                for pre in predecessors:
                    if any([adj in components.node[pre]['nodes'] for adj in adj_nodes]):
                        s_adj.append(n)
                        break
                for suc in successors:
                    if any([adj in components.node[suc]['nodes'] for adj in adj_nodes]):
                        t_adj.append(n)
                        break
            for s in s_adj:
                subgraphs[comp].add_edge('s',s)
            for t in t_adj:
                subgraphs[comp].add_edge('t',t)
    
    # Compute paths and cut_sets
    comp_cut_sets = {}
    cut_sets = {}
    pathcount = 0
    for s in starts:
        for e in ends:
            try:
                if (s,e) not in cut_sets:
                    cut_sets[(s,e)] = []
                for path in nx.all_shortest_paths(components, s, e):
                    p_cut_sets = []
                    for comp in path[1:-1]:
                        if comp not in comp_cut_sets:
                            comp_cut_sets[comp] = len(minimum_st_node_cut(subgraphs[comp],'s','t'))
                        p_cut_sets.append(comp_cut_sets[comp])
                    if len(p_cut_sets) > 0:
                        cut_sets[(s,e)].append(p_cut_sets)
            except:
                pass

    return cut_sets


def branch_thickness_v2( G ):
    # Get slabs
    slabs = {}
    for node in G:
        if not G.node[node]['isFat']:
            node_slab = G.node[node]['slab']
            if node_slab not in slabs:
                slabs[node_slab] = []
            slabs[node_slab].append(node)

    # Get the branch components
    components = nx.DiGraph()
    current_comps = []
    comp_index = 0
    for slab in slabs:
        slab_comps = nx.connected_components(G.subgraph(slabs[slab]))
        new_comps = []
        for scc in slab_comps:
            adj_comps = []
            neighbors = set()
            for n in scc:
                adj_nodes = get_location_adjacent_nodes(n)
                for adj in adj_nodes:
                    if adj in G:
                        if not G.node[adj]['isFat']:
                            neighbors.add(adj)
            for comp in current_comps:
                if any([n in components.node[comp]['nodes'] for n in neighbors]):
                    adj_comps.append(comp)
            count = len(adj_comps)

            components.add_node(comp_index, nodes=scc)
            for adj in adj_comps:
                components.add_edge(adj, comp_index)
            new_comps.append(comp_index)
            comp_index += 1
        current_comps = new_comps[:] 
    
    # Get path end points, eccen., and subgraphs
    subgraphs = {}
    starts = []
    ends = []
    critical = []
    for comp in components:
        predecessors = components.predecessors(comp)
        successors = components.successors(comp)
        pred_len = len(predecessors)
        succ_len = len(successors)
        if pred_len == 0:
            starts.append(comp)
        if succ_len == 0:
            ends.append(comp)
        if pred_len > 1 or succ_len > 1:
            critical.append(comp)

        subgraphs[comp] = G.subgraph(components.node[comp]['nodes'])
   
    is_special = lambda x: True if x in starts or x in ends or x in critical else False
    # Compute paths and cut_sets
    comp_cut_sets = {}
    cut_sets = {}
    pathcount = 0
    for start in starts:
        for end in ends:
            key = (start,end)
            try:
                for path in nx.all_shortest_paths(components, start, end):
                    if key not in cut_sets:
                        cut_sets[key] = []
                    p_cut_sets = []
                    section = []
                    s_comp = 0
                    for comp in path[1:-1]:
                        if is_special(comp):
                            t_comp = path.index(comp) 

                            section.extend(components.node[comp]['nodes'])
                            subgraph = G.subgraph(section)

                            subgraph.add_node('s')
                            subgraph.add_node('t')
                            s_adj = []
                            t_adj = []
                            for node in components.node[path[s_comp]]['nodes']:
                                adj_nodes = get_location_adjacent_nodes(node)
                                if s_comp > 0:
                                    if any([adj in components.node[path[s_comp - 1]]['nodes'] for adj in adj_nodes]):
                                        s_adj.append(node)
                                        break
                                else:
                                    if any([adj not in G.nodes() for adj in adj_nodes]):
                                        s_adj.append(node)
                                        break
                            for node in components.node[path[t_comp]]['nodes']:
                                adj_nodes = get_location_adjacent_nodes(node)
                                if t_comp < len(path) - 1:
                                    if any([adj in components.node[path[t_comp + 1]]['nodes'] for adj in adj_nodes]):
                                        t_adj.append(node)
                                        break
                                else:
                                    if any([adj not in G.nodes() for adj in adj_nodes]):
                                        t_adj.append(node)
                                        break
                            for s in s_adj:
                                subgraph.add_edge('s',s)
                            for t in t_adj:
                                subgraph.add_edge('t',t)

                            p_cut_sets.append(len(minimum_st_node_cut(subgraph,'s','t')))
                            section = []
                            s_comp = path.index(comp) 
                        else:
                            section.extend(components.node[comp]['nodes'])

                    if len(p_cut_sets) > 0:
                        cut_sets[key].append(p_cut_sets)

            except Exception, e:
                print key
                print "Error: {}".format(e)

    for key in cut_sets:
        print "{}: {}".format(key, cut_sets[key])

    return cut_sets


def branch_thickness_condensed( G ):
    # Get slabs
    slabs = {}
    for node in G:
        if not G.node[node]['isFat']:
            node_slab = G.node[node]['slab']
            if node_slab not in slabs:
                slabs[node_slab] = []
            slabs[node_slab].append(node)

    # Get the branch components
    components = nx.DiGraph()
    current_comps = []
    comp_index = 0
    for slab in slabs:
        slab_comps = nx.connected_components(G.subgraph(slabs[slab]))
        new_comps = []
        for scc in slab_comps:
            adj_comps = []
            neighbors = set()
            for n in scc:
                adj_nodes = get_location_adjacent_nodes(n)
                for adj in adj_nodes:
                    if adj in G:
                        if not G.node[adj]['isFat']:
                            neighbors.add(adj)
            for comp in current_comps:
                if any([n in components.node[comp]['nodes'] for n in neighbors]):
                    adj_comps.append(comp)
            count = len(adj_comps)

            components.add_node(comp_index, nodes=scc)
            for adj in adj_comps:
                components.add_edge(adj, comp_index)
            new_comps.append(comp_index)
            comp_index += 1
        current_comps = new_comps[:] 
    
    # Get path end points and critical points
    starts = []
    ends = []
    critical = []
    check = []
    for comp in components:
        is_special = False
        predecessors = components.predecessors(comp)
        successors = components.successors(comp)
        pred_len = len(predecessors)
        succ_len = len(successors)
        if pred_len == 0:
            starts.append(comp)
            is_special = True
        if succ_len == 0:
            ends.append(comp)
            is_special = True
        if pred_len > 1 or succ_len > 1:
            critical.append(comp)
            is_special = True
        if not is_special:
            check.append(comp)

    # Consoldate nodes
    move_to = {}
    for comp in check:
        predecessors = components.predecessors(comp)
        successors = components.successors(comp)
        pred_len = len(predecessors)
        succ_len = len(successors)
        pred = predecessors[0]
        succ = successors[0]
        if pred in check:
            if pred not in move_to:
                move_to[comp] = pred
                components.node[pred]['nodes'].extend(components.node[comp]['nodes'])
                components.add_edge(pred, succ)
            else:
                moved_pred = move_to[pred]
                components.node[moved_pred]['nodes'].extend(components.node[comp]['nodes'])
                components.add_edge(moved_pred, succ)
            components.remove_node(comp)

    subgraphs = {}
    for comp in components:
        subgraphs[comp] = G.subgraph(components.node[comp]['nodes'])
        predecessors = components.predecessors(comp)
        successors = components.successors(comp)
        if comp not in starts and comp not in ends:
            subgraphs[comp].add_node('s')
            subgraphs[comp].add_node('t')
            s_adj = []
            t_adj = []
            for n in components.node[comp]['nodes']:
                adj_nodes = get_location_adjacent_nodes(n)
                for pre in predecessors:
                    if any([adj in components.node[pre]['nodes'] for adj in adj_nodes]):
                        s_adj.append(n)
                        break
                for suc in successors:
                    if any([adj in components.node[suc]['nodes'] for adj in adj_nodes]):
                        t_adj.append(n)
                        break
            for s in s_adj:
                subgraphs[comp].add_edge('s',s)
            for t in t_adj:
                subgraphs[comp].add_edge('t',t)
    
    # Compute paths and cut_sets
    comp_cut_sets = {}
    cut_sets = {}
    paths = {}
    pathcount = 0
    for s in starts:
        for e in ends:
            key = (s,e)
            try:
                for path in nx.all_simple_paths(components, s, e):
                    if key not in cut_sets:
                        cut_sets[key] = []
                    if key not in path:
                        paths[key] = []
                    p_cut_sets = []
                    for comp in path[1:-1]:
                        if comp not in comp_cut_sets:
                            comp_cut_sets[comp] = len(minimum_st_node_cut(subgraphs[comp],'s','t'))
                        p_cut_sets.append(comp_cut_sets[comp])
                    cut_sets[key].append(p_cut_sets)
                    paths[key].append(path)
            except:
                pass

    return cut_sets


# Misc.
def get_dimensions( data ):
    mxx, mxy, mxz = data[0][1:]
    mnx, mny, mnz = data[1][1:]
    for d in data:
        if d[1] > mxx:
            mxx = d[1]
        if d[1] < mnx:
            mnx = d[1]

        if d[2] > mxy:
            mxy = d[2]
        if d[2] < mny:
            mny = d[2]

        if d[3] > mxz:
            mxz = d[3]
        if d[3] < mnz:
            mnz = d[3]

    return (mxx, mxy, mxz), (mnx, mny, mnz)


def get_content( file_name=data_set ):
    mat_contents = sio.loadmat(file_name)
    contents = mat_contents['Subjects']
    return contents


def main():
    global data_set
    contents = get_content(data_set)

    # Iterate through data set
    global subjects
    global muscles
    global fat_range

    for subject in subjects[0:1]:
        print "Subject: {}".format(subject)
        for muscle in muscles[2:3]:
            # Parameterization of the prox-dist line
            norm = contents[subject][0][0][muscle][0][0]['nVec'][0][0][0]
            intercept = contents[subject][0][0][muscle][0][0]['endPoint'][0][0][0]
            prox = contents[subject][0][0][muscle][0][0]['endPoint'][0][0][0]
            dist = contents[subject][0][0][muscle][0][0]['endPoint'][0][0][1]
            unitNorm = contents[subject][0][0][muscle][0][0]['norm_nVec'][0][0][0]

            array = contents[subject][0][0][muscle][0][0]['dataPoints'][0][0]

            # Compute the total number of slices
            length = magnitude(norm)
            num_slices = length / slab_width

            # Compute angle from Y axis
            angleFromY = norm[1] / length
            angleFromY = math.acos(angleFromY) * 180.0 / math.pi

            for fat_cutoff in [6]:#fat_range:

                global sharp_distinction
                sharp_distinction = fat_cutoff / 100.

                G = create_network(array)

                bound = get_boundary(G)
                if edge_creation == 'Sharp':
                    #fat, mus, tot, fatVoxels, muscleVoxels, fat_sizes, muscle_sizes = count_connected_components(G)
                    #fat_diam, muscle_diam = compute_diameters(G)
                    total, onbound = cc_on_boundary(G,bound)
                    print "{}, {}, {}".format(fat_cutoff,total,onbound)
                elif edge_creation == 'Gradient':
                    print nx.clustering(G)

                if visualization == '2D':
                    for p_slice in [30,50,70,90,110,130,150]:#range(70,71):#int(num_slices)+1):
                        draw_perp_slab(G, p_slice, norm, intercept, length, unitNorm, bound)
                else:
                    draw_full_muscle(G,bound)


if __name__ == '__main__':
    main()
