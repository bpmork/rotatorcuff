# Version 0.1 README #

### What is this repository for? ###

This repository is for a 3D MRI rotator cuff research project. The goal of the project is to create a pipeline that analyzes 3D MRI images as a network utilizing graph-theoretical methods.

### How do I get set up? ###

Dependancies:

* [networkx](https://networkx.github.io/)

* [scipy](http://www.scipy.org/)

* [matlibplot](http://matplotlib.org/)

### Who do I talk to? ###

Yang Ho (yho@ncsu.edu)

Brandon Mork (bpmork@ncsu.edu)