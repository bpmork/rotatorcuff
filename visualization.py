import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import math

from utils import *
from rotator_cuff import *

from mpl_toolkits.mplot3d import Axes3D

componentColors = 'bgrcmykw'

def draw_perp_slab( G, depth, norm, intercept, length, unit_norm, bound, subject=""):
    """Returns list of names of all nodes in specified perp slab.
    Uses edge_creation, sharp_distinction, gradient_distinction options from top of script.

    :param G: type nx Graph: NetworkX graph representing muscle

    :return: type list: list of names of all nodes in specified y slice.
    """

    print "Finding slab {}".format(depth)

    total_ccs = float(nx.number_connected_components(G))
    # nodes in slab
    proj_plane = intercept + ((depth + 0.5 * slab_width) / length) * norm
    nodes = []
    for node in G.nodes():
        if G.node[node]['slab'] == depth:
            nodes.append(node)

    # Coordinate transform matrix
    x = np.array([1, 0, 0])
    y = np.array([0, 1, 0])
    z = np.array([0, 0, 1])

    zp = norm
    z_ref = np.array([0, 0, 1])
    yp = projection(z_ref, unit_norm)
    xp = np.cross(zp, yp)

    xp = xp / magnitude(xp)
    yp = yp / magnitude(yp)
    zp = zp / magnitude(zp)

    transform_matrix = np.matrix([[np.dot(xp,x), np.dot(xp,y), np.dot(xp,z)],
                            [np.dot(yp,x), np.dot(yp,y), np.dot(yp,z)],
                            [np.dot(zp,x), np.dot(zp,y), np.dot(zp,z)]])

    # containers
    positions = dict()          # positions[ node ] = projected coordinates
    sizes = list()        # size of each node; must be in same order as 'nodes' list
    edges = list()        # edges in the slice
    node_colors = list()  # color of each node; must be in same order as 'nodes' list
    edge_colors = list()  # color of each edge; must be in same order as 'edges' list

    # supporting variables
    default_size = 10
    default_space = 5 * default_size

    #GP = G.subgraph(nodes)

    display_nodes = []

    # fill containers
    global edge_creation
    if edge_creation == 'Sharp':
        global sharp_distinction
        for node in nodes:

            distance = abs(np.dot(node - proj_plane, unit_norm))
            projected_node = projection(node, unit_norm)
            transformed_node = transform_matrix * np.matrix([[projected_node[0]], [projected_node[1]], [projected_node[2]]])

            display_nodes.append(node)
            positions[node] = [default_space * transformed_node.item(0), default_space * transformed_node.item(1)]
            sizes.append(default_size ** (node_size(G, node) * 1.00))#* (1 - distance) * 2.5)

            node_is_boundary = node in bound
            node_is_border = G.node[node]['border']

            if G.node[node]['isFat']:#['fatFraction'] <= sharp_distinction:
                if node_is_boundary or node_is_border:
                    node_color = 0.8
                else:
                    node_color = 0.6
                edge_color = 0.6
            else:
                if node_is_boundary or node_is_border:
                    node_color = 0.1
                else:
                    node_color = 0.2
                edge_color = 0.1
            node_colors.append(node_color)
            #node_colors.append(componentColors[G.node[node]['Component'] % 8])

            if node_is_boundary or node_is_border:
                # For grey colors use: '0.1' and '1.0' for nodes and
                # '0.1' and '0.6' for edges
                neighborBound = False
                for neighbor in G.neighbors(node):
                    if neighbor in nodes:
                        neighbor_is_boundary = neighbor in bound
                        neighbor_is_border = G.node[neighbor]['border']
                        if (node_is_boundary and neighbor_is_boundary) or (node_is_border and neighbor_is_border):
                            edges.append((node, neighbor))
                            edge_colors.append(edge_color)
                            #edge_colors.append(componentColors[G.node[node]['Component'] % 8])
                            neighborBound = True

    elif edge_creation == 'Gradient':
        global gradient_distinction
        global minimum
        global maximum
        relative_max = maximum
        for node in nodes:
            display_nodes.append(node)

            distance = abs(np.dot(node - proj_plane, unit_norm))
            projected_node = projection(node, unit_norm)
            transformed_node = transform_matrix * np.matrix([[projected_node[0]], [projected_node[1]], [projected_node[2]]])

            positions[node] = [default_space * transformed_node.item(0), default_space * transformed_node.item(1)]
            sizes.append(default_size ** (node_size(G, node) * 1.00))
            node_color = G.node[node]['fatFraction']
            node_colors.append(node_color)
            for neighbor in G.neighbors(node):
                if neighbor in nodes:
                    edges.append((node, neighbor))
                    edge_color = ((G.node[node]['fatFraction'] + G.node[neighbor]['fatFraction']) / 2.0)# - minimum) * ((1 - minimum) / relative_max)
                    neighbor_values = neighbor
                    edge_colors.append(edge_color)

    if len(nodes) > 0:
        if visualization == '3D':
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            muscle_x = []
            muscle_y = []
            muscle_z = []
            fat_x = []
            fat_y = []
            fat_z = []
            for node in nodes:
                if G.node[node]['isFat']:#['fatFraction'] <= sharp_distinction:
                    fat_x.append(node[0])
                    fat_y.append(node[1])
                    fat_z.append(node[2])
                else:
                    muscle_x.append(node[0])
                    muscle_y.append(node[1])
                    muscle_z.append(node[2])

            ax.scatter(muscle_x,muscle_y,muscle_z,c='r')
            ax.scatter(fat_x,fat_y,fat_z,c='b')
        else:
            fig = plt.figure()
            if edge_creation == 'Gradient':
                nx.draw_networkx_nodes(G, positions, nodelist=display_nodes, node_size=sizes, node_color=node_colors, node_shape='o', cmap=plt.cm.Reds_r)
                nx.draw_networkx_edges(G, positions, edgelist=edges, edge_color=edge_colors, edge_cmap=plt.cm.Reds_r, width=2)
            else:
                nx.draw_networkx_nodes(G, positions, nodelist=display_nodes, node_size=sizes, node_color=node_colors, node_shape='o', cmap=plt.cm.RdBu)
                nx.draw_networkx_edges(G, positions, edgelist=edges, edge_color=edge_colors, edge_cmap=plt.cm.RdBu, width=2)
            fig.suptitle("Subject: {}, Slab: {}".format(subject, depth))
            plt.ylim([000,100])
            plt.xlim([000,100])
            plt.axis('off')
            plt.axis('equal')
        plt.show()


def draw_y_slice( G, depth ):
    """Returns list of names of all nodes in specified y slice.
    Uses edge_creation, sharp_distinction, gradient_distinction options from top of script.

    :param G: type nx Graph: NetworkX graph representing muscle

    :return: type list: list of names of all nodes in specified y slice.

    Author: Brandon Mork
    """

    # nodes in y slice
    nodes = [node  for node in G.nodes()  if int(node[3:6]) == depth]

    # containers
    positions = dict()          # positions[node  = [x, z]
    sizes = list()        # size of each node; must be in same order as 'nodes' list
    edges = list()        # edges in the slice
    node_colors = list()  # color of each node; must be in same order as 'nodes' list
    edge_colors = list()  # color of each edge; must be in same order as 'edges' list

    # supporting variables
    default_size = 10
    default_space = 2 * default_size

    # fill containers
    global edge_creation
    if edge_creation == 'Sharp':
        global sharp_distinction
        for node in nodes:
            positions[node] = [default_space * int(node[:3]), default_space * int(node[-3:])]
            sizes.append(default_size * node_size(G, node))
            if G.node[node]['fatFraction'] <= sharp_distinction:
                node_colors.append('0.1')
                edge_color = '0.1'
            else:
                node_colors.append('1.0')
                edge_color = '0.6'

    elif edge_creation == 'Gradient':
        global gradient_distinction
        global minimum
        global maximum
        relative_max = maximum
        for node in nodes:
            positions[node] = [default_space * int(node[:3]), default_space * int(node[-3:])]
            sizes.append(default_size * node_size(G, node))
            node_color = G.node[node]['fatFraction'] * 2
            if node_color > 1:
                node_color = '1.0'
            else:
                node_color = str(node_color)
            for neighbor in G.neighbors(node):
                if neighbor in nodes:
                    edges.append((node, neighbor))
                    edge_color = ((G.node[node]['fatFraction'] + G.node[neighbor]['fatFraction']) / 2 - minimum) * ((1 - minimum) / relative_max)
                    if edge_color > 1:
                        edge_color = '1.0'
                    else:
                        edge_color = str(edge_color)
                    edge_colors.append(edge_color)
            node_colors.append(node_color)

    if len(nodes) > 0:
        nx.draw_networkx_nodes(G, positions, nodelist=nodes, node_size=sizes, node_color=node_colors, node_shape='o', linewidths=1)
        nx.draw_networkx_edges(G, positions, edgelist=edges, edge_color=edge_colors, width=2)
        plt.axis('off')
        plt.axis('equal')
        plt.show()


def draw_full_muscle( Comps, boundary ):
    mColor = 0.1
    fColor = 0.1
    fcolors = []
    mcolors = []
    updateColor = "m"
    node_x = []
    node_y = []
    node_z = []
    colors = []
    for cc in Comps:
        for node in cc.nodes():
            node_x.append(node[0])
            node_y.append(node[1])
            node_z.append(node[2])
            if cc.node[node]['isFat']:#['fatFraction'] <= sharp_distinction:
                colors.append((0,0,fColor))
                updateColor = "f"
            else:
                colors.append((mColor,0,0))
                updateColor = "m"
        if updateColor == "m":
            mColor += .1
            if mColor > 1.0:
                mColor = 0.1
        elif updateColor == "f":
            fColor += .1
            if fColor > 1.0:
                fColor = 0.1

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(node_x,node_y,node_z,c=colors,marker=',',alpha=0.35)
    ax.set_xlim3d(61,192)
    ax.set_ylim3d(23,233)
    ax.set_zlim3d(140,229)
    #plt.show()


def draw_component( Comp ):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    node_x, node_y, node_z = zip(*Comp.nodes())

    node = Comp.nodes()[0]

    color = 'b' if Comp.node[node]['isFat'] else 'r'
    ax.scatter(node_x,node_y,node_z,c=color ,marker='.')
    ax.set_xlim3d(61,192)
    ax.set_ylim3d(23,233)
    ax.set_zlim3d(140,229)
    plt.show()


def draw_muscle_components( Comps, boundary, subject, volume ):
    node_x = []
    node_y = []
    node_z = []
    colors = []
    color = ['y','r','b']
    colorIndex = 0
    toRemove = []
    for cc in Comps:
        ccSize = 100.0 * float(cc.number_of_nodes())/ volume
        if ccSize <= 0.01:
            colorIndex = 0
        elif ccSize <= 1.00:
            colorIndex = 1
        else:
            colorIndex = 2

        for node in cc.nodes():
            if not cc.node[node]['isFat']:
                node_x.append(node[0])
                node_y.append(node[1])
                node_z.append(node[2])
                colors.append(color[colorIndex])
                if node in boundary:
                    toRemove.append(node)

    bound = boundary.copy()
    bound.remove_nodes_from(toRemove)
    BX, BY, BZ = zip(*bound)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(node_x,node_y,node_z,c=colors,marker=',')
    ax.scatter(BX,BY,BZ,c=(1.00,1.00,1.00),alpha=0.20,marker=',',linewidth=1.0, s=10.0)
    ax.set_xlim3d(61,192)
    ax.set_ylim3d(23,233)
    ax.set_zlim3d(140,229)
    fig.suptitle("Subject: {}, Muscle".format(subject))
    #plt.show()


def draw_fat_components( Comps, boundary, subject, volume ):
    node_x = []
    node_y = []
    node_z = []
    colors = []
    color = ['y','r','b']
    colorIndex = 0
    toRemove = []
    for cc in Comps:
        ccSize = 100.0 * float(cc.number_of_nodes())/ volume
        if ccSize <= 0.01:
            colorIndex = 0
        elif ccSize <= 1.00:
            colorIndex = 1
        else:
            colorIndex = 2

        for node in cc.nodes():
            if cc.node[node]['isFat']:
                node_x.append(node[0])
                node_y.append(node[1])
                node_z.append(node[2])
                colors.append(color[colorIndex])
                if node in boundary:
                    toRemove.append(node)

    bound = boundary.copy()
    bound.remove_nodes_from(toRemove)
    BX, BY, BZ = zip(*bound)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(node_x,node_y,node_z,c=colors,marker=',')
    ax.scatter(BX,BY,BZ,c=(1.00,1.00,1.00),alpha=0.20,marker=',',linewidth=1.0, s=10.0)
    ax.set_xlim3d(61,192)
    ax.set_ylim3d(23,233)
    ax.set_zlim3d(140,229)
    fig.suptitle("Subject: {}, fat".format(subject))
    #plt.show()


def draw_prox_dist( boundary, subject, prox, dist ):
    BX, BY, BZ = zip(*boundary)
    node_x, node_y, node_z = zip(prox, dist)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(prox[0],prox[1],prox[2],c='r',marker=',')
    ax.scatter(dist[0],dist[1],dist[2],c='b',marker=',')
    ax.plot(node_x,node_y,node_z)
    ax.scatter(BX,BY,BZ,c=(1.00,1.00,1.00),alpha=0.20,marker=',',linewidth=1.0, s=10.0)
    #ax.set_xlim3d(61,192)
    #ax.set_ylim3d(23,233)
    #ax.set_zlim3d(140,229)
    fig.suptitle("Subject: {}, fat".format(subject))
    #plt.show()


def draw_branch_structure( components ):
    node_colors = [] 
    nodes = []
    for comp in components:
        color = ""
        if comp in critical:
            if comp in starts:
                color = 0.25 
            elif comp in ends:
                color = 0.75 
            else:
                color = 0.5 
        elif comp not in check:
            if comp in starts:
                if comp in ends:
                    color = .67 
                else:
                    color = 0 
            elif comp in ends:
                color = 1 
        else:
            color = .33
        node_colors.append(color)
        nodes.append(comp)

    nx.write_dot(components, 'test.dot')
    positions = nx.graphviz_layout(components,prog='dot')
    nx.draw_networkx(components, positions, nodelist=nodes, node_color = node_colors, cmap=plt.cm.brg)
    plt.show()
