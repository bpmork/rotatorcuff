      | prox   | mid    | dist
total | 0.0927 | 0.0363 | 0.3954
vs    | 0.1314 | 0.0418 | 0.4468
s     | 0.7982 | 0.4981 | 0.4299

GUIDE:

total = all components
vs = just very small
s = just small
